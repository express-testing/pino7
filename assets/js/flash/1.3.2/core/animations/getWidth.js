flashCore.prototype.getWidth = function(el) {
    var el_style      = window.getComputedStyle(el),
        el_display    = el_style.display,
        el_position   = el_style.position,
        el_visibility = el_style.visibility,
        el_transition = el.style['transition'],
        el_max_height = el_style.maxHeight,
        el_max_width = el_style.maxHeight,

        wanted_width = 0;


    // if its not hidden we just return normal height
    if(el_display !== 'none' && el_max_height.replace('px', '').replace('%', '') !== '0') {
        return el.offsetWidth;
    }

    // the element is hidden so:
    // making the el block so we can meassure its height but still be hidden
    el.style.position   = 'absolute';
    el.style.visibility = 'hidden';
    el.style.display    = 'block';
    el.style.transition = '';
    el.style.maxHeight  =  'none';
    el.style.maxWidth  =  'none';
    var parent_position = el.parentNode.style.position;
    if(!parent_position) {
        el.parentNode.style.position = 'relative';
    }

    wanted_height     = el.offsetWidth;

    // reverting to the original values
    el.parentNode.style.position = parent_position;
    el.style.display    = el_display;
    el.style.position   = el_position;
    el.style.visibility = el_visibility;
    el.style.transition = el_transition;
    el.style.maxHeight = el_max_height;
    el.style.maxWidth = el_max_width;
    
    return wanted_width;
};