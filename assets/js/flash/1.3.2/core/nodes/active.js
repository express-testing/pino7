/**
 * v 1.0
 * ADDING "ACTIVE" CLASS TO ANCHORS AND TAGS BASED ON CURRENT URL
 * 
 *
 * Exaples of usage:
 * 
 * 	<a href="/mypage" data-active>Link</a>   								<--- To target a single link
 *
 * 
 *	<div data-active-children>      										<--- To add a list of links in a tag
 *		<a href="/mypage" data-active-self>Link</a>							<--- To set active just on this page and not in childern
 *		<a href="/mypage2" data-active-ignore>Link</a>   					<--- To ignore a link in a list
 *		<a href="/mypage/abc">Link</a>   	
 *		<a href="/mypage2" data-active-exclude="/mypage2/fgh">Link</a>   	<--- To exclude a subpage in particular
 *		<a href="/mypage2/fgh">Link</a>   
 *		<a href="/mypage2/#fgh" data-active-self-strict>Link</a>   			<--- To match just the specific hash				
 *	</div>
 *
 *
 *	<div data-active-on="/my-url|/my-url-2">Fake Link</div>   				<--- To target any tag
 */


flashCore.prototype.activeLinks = function() {
	// Prepares the collection of elements
	var links = document.querySelectorAll('[data-active]:not([data-active-ignore]), [data-active-children] a:not([data-active-ignore]), [data-active-on]:not([data-active-ignore])');

	// Stops if there are no links to watch for
	if (!links.length) {
		return;
	}

	var page_url = typeof flash_original_url == 'undefined' ? window.location.pathname : flash_original_url;

	links.forEach( function(link){
	   	var matches = false;
		
		// Check if already applied
		link.classList.add('js_init--flashActive');

		// Gets the url of the current element
		if (link.getAttribute('data-active-on')) {
			var link_urls = link.getAttribute('data-active-on').split('|');
		} else {
			var link_urls = [link.getAttribute('href')];
		}

		if (link_urls) {
			link_urls.forEach(function( link_url ){
				switch(link_url) {
					// If the link url is to the home
					case '/':
						matches = (page_url === link_url || window.location.pathname === link_url);
						break;
					case '#':
						matches = false;
						break;
					// If the link url is to any other page
					default:
						if(link.hasAttribute('data-active-self')) {
							matches = (window.location.pathname === link_url || (link_url.split('#'))[0] === window.location.pathname);
						} else if( link.hasAttribute('data-active-exclude') ){
							matches = ( window.location.pathname.indexOf(link_url) >= 0 && window.location.pathname.indexOf(link.getAttribute('data-active-exclude')) == -1 );
						} else {
							matches = matches || (page_url.indexOf((link_url.split('#'))[0]) >= 0 ||  window.location.pathname.indexOf((link_url.split('#'))[0]) >= 0);
						}

						// If the url must be the same, with no flexibility on the hash (if any hash is present)
						if(link.hasAttribute('data-active-self-strict')) {
							matches = (window.location.pathname + window.location.hash) === link_url;
						}
						break;
				}
			});

			// If url matches, the class is applied
			if (matches) {
				link.classList.add('active');
			}
		}
	});
}